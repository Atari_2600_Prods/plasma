
.include "vcs.inc"
.include "globals.inc"

.define JOYDELAY 25
.define PFFBLINESIZE 7

.segment "ZEROPAGE"

line:
   .byte $00
plasmatemp:
   .byte $00
flashcolors:
   .byte $00,$00,$00,$00,$00,$00,$00

.segment "RODATA"

plasmas:
   .word plasma0,  plasma1,  plasma2,  plasma3
   .word plasma4,  plasma5,  plasma6,  plasma7
   .word plasma8,  plasma9,  plasma10, plasma11
   .word plasma12, plasma13, plasma14, plasma15

.segment "CODE"
;.align 256

selectplasma:
   lda config
   and #$f0
   lsr
   lsr
   lsr
   tax
   lda plasmas,x
   sta temp1
   lda plasmas+1,x
   sta temp2
   ldx startx
   ldy starty
   jmp (temp1)


plasma1:
; c=(abs(y)+sin(x)), x+2, y+1 
   jsr waitvblank
@loop:             
   tya
   bmi @notoggle
   eor #$ff
@notoggle:
   clc
   adc sinustab,x
   sty temp1
   tay
   lda (colortab),y
   ldy temp1
   sta WSYNC
   sta COLUBK
   iny
   inx
   inx
   dec line
   bne @loop
   ldx temp2
   txs
   jmp displayconfig

plasma2:
; c=(.5fy+1.5fx), y+3, x+2
   jsr waitvblank
@loop:
   lda sinustab,y
   adc sinustab,x
   lsr
   clc
   adc sinustab,x
   sty temp1
   tay
   lda (colortab),y
   ldy temp1
   sta WSYNC
   sta COLUBK
   iny
   iny
   iny
   inx
   inx
   dec line
   bne @loop
   jmp displayconfig

plasma3:
; c=(.5fy+1.5fx), y+2, x+3
   jsr waitvblank
@loop:
   lda sinustab,y
   adc sinustab,x
   lsr
   clc
   adc sinustab,x
   sty temp1
   tay
   lda (colortab),y
   ldy temp1
   sta WSYNC
   sta COLUBK
   iny
   iny
   inx
   inx
   inx
   dec line
   bne @loop
   jmp displayconfig
   
plasma4:
; c=fy/2+fx, x+3, y-1
; c+=(frame & $f0)
   jsr waitvblank
@loop:
   lda sinustab,y
   lsr
   adc sinustab,x
   sty temp1
   sta temp2
   lda frame
   and #$f0
   adc temp2
   tay
   lda (colortab),y
   ldy temp1
   sta WSYNC
   sta COLUBK
   dey
   inx
   inx
   inx
   dec line
   bne @loop
   sta WSYNC
   jmp displayconfig

plasma5:
; c=(fy+fx), x+2, y+1
   jsr waitvblank
@loop:
   lda sinustab,y
   clc
   adc sinustab,x
   sty temp1
   tay
   lda (colortab),y
   ldy temp1
   sta WSYNC
   sta COLUBK
   iny
   inx
   inx
   dec line
   bne @loop
   sta WSYNC
   jmp displayconfig

plasma6:
; c=(s(x)+s(y)-frame), x+=1.5 y-=0.5
   jsr waitvblank
@loop:
   lda sinustab,y
   clc
   adc sinustab,x
   sec
   sbc frame
   sty temp1
   tay
   lda (colortab),y
   ldy temp1
   sta WSYNC
   sta COLUBK
   sta temp2
   lda line
   and #$01
   bne @noy
   dey
   inx
@noy:
   inx
   dec line
   bne @loop
   sta WSYNC
   lda temp2
   jmp displayconfig

plasma7:
; c=(fy+fx), x+2, y-1
   lda #$01
   sta temp2
   jsr waitvblank
@loop:
   lda sinustab,y
   clc
   adc sinustab,x
   sec
   sbc frame
   sty temp1
   tay
   lda (colortab),y
   ldy temp1
   sta WSYNC
   sta COLUBK
   dec temp2
   bne @noy
   dey
   lda #$03
   sta temp2
@noy:
   inx
   dec line
   bne @loop
   sta WSYNC
   jmp displayconfig

plasma8:
; c=2fy-fx, y+1, x+2
   jsr waitvblank
@loop:
   lda sinustab,y
   asl
   sec
   sbc sinustab,x
   sty temp1
   tay
   lda (colortab),y
   ldy temp1
   sta WSYNC
   sta COLUBK
   iny
   inx
   inx
   dec line
   bne @loop
   sta WSYNC
   jmp displayconfig
   
plasma9:
; c=(2fy-fx), x+2, y+.5
   jsr waitvblank
@loop:
   lda sinustab,y
   asl
   sec
   sbc sinustab,x
   sty temp1
   tay
   lda (colortab),y
   ldy temp1
   sta WSYNC
   sta COLUBK
   inx
   inx
   lda line
   and #$01
   bne @noy
   iny
@noy:
   dec line
   bne @loop
   lda (colortab),y
   jmp displayconfig
   
plasma10:
; c=(fy+fx), x+2, y-1
   jsr waitvblank
@loop:
   lda sinustab,y
   clc
   adc sinustab,x
   sty temp1
   tay
   lda (colortab),y
   ldy temp1
   sta WSYNC
   sta COLUBK
   dey
   inx
   inx
   dec line
   bne @loop
   sta WSYNC
   jmp displayconfig
   
plasma11:
; c=(2fy-fx), x-1/3, y+1/2   
   jsr waitvblank
   lda #$02
   sta temp2
@loop:
   lda sinustab,y
   asl
   sec
   sbc sinustab,x
   sty temp1
   tay
   lda (colortab),y
   ldy temp1
   sta WSYNC
   sta COLUBK
   dec temp2
   bne @nox
   lda #$02
   sta temp2
   dex
@nox:
   lda line
   and #$01
   bne @noy
   iny
@noy:
   dec line
   bne @loop
   lda (colortab),y
   jmp displayconfig
   
plasma12:
; c=(($100-fy)*2-fx), x-1, y+1
   jsr waitvblank
@loop:
   lda #$00
   sec
   sbc sinustab,y
   asl
   sbc sinustab,x
   sty temp1
   tay
   lda (colortab),y
   ldy temp1
   sta WSYNC
   sta COLUBK
   dex
   iny
   dec line
   bne @loop
   sta WSYNC
   jmp displayconfig

plasma13:
; c=((fy+fx) & $0f) | (y & $f0), x+3, y-1
   jsr waitvblank
   lda seedx
   sta temp2
@loop:
   clc
   lda sinustab,x
   adc sinustab,y
   and #$0f
   sta temp2
   tya
   and #$f0
   ora temp2
   sty temp1
   tay
   lda (colortab),y
   ldy temp1
   sta WSYNC
   sta COLUBK
   dey
   inx
   inx
   inx
   dec line
   bne @loop
   jmp displayconfig

plasma14:
; g($ab)=$ba
; c=((g(fy)+fx), x+2, y+1
   jsr waitvblank
@loop:
   lda sinustab,y
   asl
   adc #$00
   asl
   adc #$00
   asl
   adc #$00
   asl
   adc #$00
   clc
   adc sinustab,x
   sty temp1
   tay
   lda (colortab),y
   ldy temp1
   sta WSYNC
   sta COLUBK
   iny
   inx
   inx
   dec line
   bne @loop
   sta WSYNC
   jmp displayconfig

plasma15:
   txa
   clc
   adc #$40
   tax
   tya
   clc
   adc #$60
   tay
   jsr waitvblank
@loop1:
   lda sinustab,x
   asl
   sec
   sbc sinustab,y
   sty temp1
   tay
   lda (colortab),y
   ldy temp1
   sta WSYNC
   sta COLUBK
   sta temp2
   lda line
   ;eor startx
   ;eor starty
   and #$20
   beq @route2
   dey
   inx
   inx
   jmp @routedone
@route2:
   iny
   dex
   dex
@routedone:
   dec line
   bne @loop1
   lda temp2
   sta WSYNC
   jmp displayconfig

plasma0:
; c=(fy/2+fx), y-1, x+3
; easteregg(!)
.macro plasma_calc_value
   lda sinustab,y
   lsr
   adc sinustab,x
.endmacro

.macro plasma_add_steps
   inx
   inx
   inx
   dey
.endmacro

.macro read_colortab coltab
   txs
   tax
   lda coltab,x
.endmacro

.macro read_colortab_zp
   sty temp1
   tay
   lda (colortab),y
   ldy temp1
.endmacro

.macro clear_pf
   lda #$00
   sta PF0
   sta PF1
   sta PF2
.endmacro

.macro set_pf pf
   lda pf+0
   sta PF0
   lda pf+1
   sta PF1
   lda pf+2
   sta PF2
.endmacro

.macro fbline7 coltab, lineno, crosspage
.local @loop1
   plasma_calc_value
.if 0
   read_colortab coltab
.else
   txs
   tax
   lda flashcolors + (6-lineno)
   sta COLUPF
.endif
   lda coltab,x
   sta WSYNC
@loop1:
.ifblank crosspage
   sta COLUBK | $0100
.else
   sta COLUBK
.endif
   set_pf pffb+(lineno*6)+$00
   tsx
   plasma_add_steps
   set_pf pffb+(lineno*6)+$03
   plasma_calc_value
   read_colortab coltab
   dec line
   bne @loop1
   sta COLUBK
   lda #$00
   sta PF0
   sta PF1
   sta PF2
   lda #PFFBLINESIZE
   sta line
   tsx
   plasma_add_steps
.endmacro

   lda line
   sec
   sbc #$40
   sta line

.if 0
   stx temp1
   tsx
   stx temp2
   ldx temp1
.else   
   txa
   pha
   tya
   pha

   ldy #$06
@loop:
   tya
   asl
   adc frame
   and #$e0
   bne @nocol
   tya
   asl
   adc frame
   lsr
   clc
   and #$0f
   adc #$20
   tax
   lda paltab,x
   .byte $2c
@nocol:
   lda #$00   
   sta flashcolors,y
   dey
   bpl @loop

   pla
   tay
   pla
   sta temp1
   tsx
   stx temp2
   ldx temp1
.endif

   jsr waitvblank
   
@toploop:
   plasma_calc_value
   sty temp1
   tay
   lda (colortab),y
   ldy temp1
   sta WSYNC
   sta COLUBK
   plasma_add_steps
   dec line
   bne @toploop
   
   lda #PFFBLINESIZE
   sta line
   lda SWCHB
   bpl @pal
   jmp @ntsc

@pal:
   fbline7 paltab, 0
   fbline7 paltab, 1
   fbline7 paltab, 2
   fbline7 paltab, 3
   fbline7 paltab, 4
   fbline7 paltab, 5
   fbline7 paltab, 6
   jmp @done
   
@ntsc:
   fbline7 ntsctab, 0
   fbline7 ntsctab, 1
   fbline7 ntsctab, 2
   fbline7 ntsctab, 3
   fbline7 ntsctab, 4
   fbline7 ntsctab, 5
   fbline7 ntsctab, 6

@done:
   lda #$08
   sta line
   lda #$00
   sta PF0
   sta PF1
   sta PF2

@bottomloop:
   plasma_calc_value
   sty temp1
   tay
   lda (colortab),y
   ldy temp1
   sta WSYNC
   sta COLUBK
   plasma_add_steps
   dec line
   bne @bottomloop

   ldx temp2
   txs
   jmp displayconfig
   
