
.include "vcs.inc"
.include "globals.inc"

.define JOYDELAY 25

.segment "ZEROPAGE"
frame:
   .byte $00
colortab:
   .word $0000
temp1:
   .byte $00
temp2:
   .byte $00
config:
   .byte $00
joydelay:
   .byte $00
numlines:
   .byte $00
timerblank:
   .byte $00
timeroscan:
   .byte $00
automode:
   .byte $00
pffb:
   .byte $00,$00,$00,$00,$00,$00
   .byte $00,$00,$00,$00,$00,$00
   .byte $00,$00,$00,$00,$00,$00
   .byte $00,$00,$00,$00,$00,$00
   .byte $00,$00,$00,$00,$00,$00
   .byte $00,$00,$00,$00,$00,$00
   .byte $00,$00,$00,$00,$00,$00
   .byte $00,$00,$00,$00,$00,$00
hexdata:
   .byte $00,$00,$00,$00,$00,$00,$00
colordata:
   .byte $00,$00,$00,$00,$00,$00,$00
   
.segment "RODATA"

logodata0:
.if 1
   .byte %01100001
   .byte %01100001
   .byte %01100001
   .byte %01111001
   .byte %01101101
   .byte %01101101
   .byte %11111001
.else
   .byte %00110001
   .byte %00110001
   .byte %00110001
   .byte %00111101
   .byte %00110110
   .byte %00110110
   .byte %01111101
.endif

logodata1:
   .byte %11111011
   .byte %10000011
   .byte %10011011
   .byte %10011111
   .byte %10011011
   .byte %10011011
   .byte %10001110

logodata2:
   .byte %11111001
   .byte %00001101
   .byte %00001101
   .byte %00111001
   .byte %01100001
   .byte %01100001
   .byte %00111111

logodata3:
   .byte %10001101
   .byte %10001101
   .byte %10101101
   .byte %11111101
   .byte %11011101
   .byte %10001101
   .byte %00000100

logodata4:
   .byte %10110000
   .byte %10110000
   .byte %10110000
   .byte %11110000
   .byte %10110000
   .byte %10110000
   .byte %11100000

logocolors:
.if 1
   .byte $38,$58,$78,$98,$b8,$d8,$f8
.else
   .byte $38,$48,$58,$68,$78,$88,$98
.endif

svolli:
   .byte $c0,$ff,$ff,$f0,$ff,$ff
   .byte $60,$00,$98,$d0,$0c,$24
   .byte $60,$00,$98,$d0,$0c,$18
   .byte $c0,$cc,$9b,$d0,$0c,$00
   .byte $00,$6c,$9b,$d0,$0c,$18
   .byte $00,$67,$99,$d0,$6d,$19
   .byte $f0,$c3,$f0,$80,$c7,$3c

.if 0
palconfig:
   .word paltab
   .byte $e8, $2c, $2c

ntscconfig:
   .word ntsctab
   .byte $c4, $2a, $1e
.endif

.segment "CODE"
   
tvsetup:
   ldx #$e7 ; PAL: number of lines
   lda #$2c ; PAL: timer for vblank
   tay      ; PAL: timer for overscan
   bit SWCHB
   bvc @hz60
   ldx #$c3 ; NTSC: number of lines
   lda #$2b ; NTSC: timer for vblank
   ldy #$1d ; NTSC: timer for overscan
@hz60:
   stx numlines
   sta timerblank
   sty timeroscan
   ldx #<paltab
   ldy #>paltab
   bit SWCHB
   bpl @pal 
   ldx #<ntsctab
   ldy #>ntsctab
@pal:
   stx colortab
   sty colortab+1
   rts

plasma:
   jsr tvsetup
   bit INPT5
   bmi @skipegg
   ldx #41
@loop:
   lda svolli,x
   sta pffb,x
   dex
   bpl @loop
@skipegg:

   ; detect joy
   lda SWCHB
   and #$03
   sta temp1
   lda SWCHA
   and #$f0
   ora temp1
   cmp #$f3
   bne @nojoy
   bit INPT4
   bpl @nojoy
   lda #$01
   sta joydelay
@nojoy:

   lda SWCHA
   and #$10
   bne @noup
   dec joydelay
   bne @noup
   clc
   lda config
   adc #$10
   sta config
   lda #JOYDELAY
   sta joydelay
@noup:
   lda SWCHA
   and #$20
   bne @nodown
   dec joydelay
   bne @nodown
   sec
   lda config
   sbc #$10
   sta config
   lda #JOYDELAY
   sta joydelay
@nodown:
   lda SWCHA
   and #$40
   bne @noleft
   dec joydelay
   bne @noleft
   lda config
   tax
   and #$f0
   sta config
   sec
   txa
   sbc #$01
   and #$0f
   ora config
   sta config
   lda #JOYDELAY
   sta joydelay
@noleft:
   lda SWCHA
   and #$80
   bne @noright
   dec joydelay
   bne @noright
   lda config
   tax
   and #$f0
   sta config
   clc
   txa
   adc #$01
   and #$0f
   ora config
   sta config
   lda #JOYDELAY
   sta joydelay
@noright:
   
   lda SWCHB
   and #$02
   bne @noselect
   dec joydelay
   bne @noselect
   clc
   lda config
   adc #$10
   sta config
   lda #JOYDELAY
   sta joydelay
   
@noselect:
   lda SWCHB
   and #$01
   bne @noreset
   dec joydelay
   bne @noreset
   lda config
   tax
   and #$f0
   sta config
   clc
   txa
   adc #$01
   and #$0f
   ora config
   sta config
   lda #JOYDELAY
   sta joydelay
   
@noreset:
   bit INPT4
   bmi @nobutton
   dec joydelay
   bne @nobutton
   lda #$02
   eor automode
   sta automode
   lda #JOYDELAY
   sta joydelay
   bne @instantchg
   
@nobutton:

   lda automode
   eor #$02
   ora frame
   bne @noauto

@instantchg:
   lda config
   bne @noseed
   inc config
@noseed:
   ; lfsr taken from pitfall
   lda config
   asl
   eor config
   asl
   eor config
   asl
   asl
   rol
   eor config
   lsr
   ror config

@noauto:

   lda numlines
   sta line
   inc frame

   jmp selectseed
   

displayconfig:
; audio gizmo
   tax
   lsr
   lsr
   lsr
   lsr
   sta temp1
   txa
   and #$0f
   clc
   adc temp1
   sta AUDF0
   sta AUDF1
   lda #$0c
   sta AUDC0
   lda #$04
   sta AUDC1
   asl
   and SWCHB
   eor #$08
   sta AUDV0
   sta AUDV1
   lda #$00
   sta WSYNC
   sta COLUBK
   jsr waitscreen
   sta WSYNC


bigsprite:
; display the logo and config-number   
   lda #$01
   sta VDELP0
   sta VDELP1
   lda #$03
   sta NUSIZ0
   sta NUSIZ1
;   ldy #$a7 ; 2
;   lda (colortab),y ; 5
;   sta COLUP0 ; 3
;   sta COLUP1; 3
   lda automode
   sta ENAM0
   ldx #$06
   nop
@loop:
   dex
   bne @loop
   stx RESP0
   stx RESP1
   lda #$10
   sta HMP1

   lda #logodata1-logodata0-1
   sta temp2

   sta WSYNC
   sta HMOVE
   
@spriteloop:
   ldx temp2
   nop
   lda colordata,x
   sta COLUP0
   sta COLUP1
   lda logodata0,x
   sta GRP0        ; logodata0 to H0
   lda logodata1,x
   sta GRP1        ; logodata1 to H1,   logodata0 to P0
   lda logodata2,x
   sta GRP0        ; logodata2 to H0,   logodata1 to P1
   lda hexdata,x
   sta temp1
   lda logodata4,x
   tay
   lda logodata3,x
   ldx temp1
   sta GRP1        ; logodata3 to H1,   logodata2 to P0
   sty GRP0        ; logodata4 to H0,   logodata3 to P1
   stx GRP1        ; hexdata   to H1,   logodata4 to P0
   sty GRP0        ; (logodata4 to H0), hexdata   to P1
   dec temp2
   bpl @spriteloop
   
   ldy #$00
   sty GRP1        ; $00 to H1,   (logodata4 to P0)
   sty GRP0        ; $00 to H0,   $00 to P1
   sty GRP1        ; ($00 to H1), $00 to P0
   sty NUSIZ0
   sty NUSIZ1
   sty ENAM0
;   ldy #$04
;@resync:
;   dey
;   bpl @resync
   sty WSYNC

;calculate next config-number
   ldy #$07
@hexloop:
   lda config
   and #$f0
   lsr       ; clc
   sta temp1
   tya
   adc temp1
   tax
   lda hexnumbers,x
   and #$f0
   sta temp2
   lda config
   and #$0f
   asl
   asl
   asl
   sta temp1
   tya
   adc temp1
   tax
   lda hexnumbers,x
   and #$0f
   ora temp2
   sta hexdata-1,y
   dey
   bne @hexloop
   
   ldx #$06
@colorloop:
   lda logocolors,x
   tay
   lda (colortab),y
   sta colordata,x
   dex
   bpl @colorloop
   jmp waitoverscan
