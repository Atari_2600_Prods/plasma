
.include "vcs.inc"
.include "globals.inc"

.segment "ZEROPAGE"

; gfx are written to $90-$ec

.segment "CODE"

splash:
   lda frame
   bne @noinit
   ldx #$01   ; X on purpose, will be used in copy below
   stx CTRLPF ; 

   lda #%00110000
@gfxloop:   
   tay
   ora #$80
@loop1:
   sta $8f,x  ; copy to $90-$ec, X starts with 1, see above
   inx
   dey
   bne @loop1 ; from here on Y=$00, assumed below
   and #$7f
   lsr
   cmp #%00000001
   bne @gfxloop
   lda #$02
   sta AUDC0
   lda #$1f
   sta AUDF0
   lda #$fc
   sta AUDV0
   sta frame

@noinit:
   cmp #$fc
   bne @decframe
   bit INPT4
   bpl @noframe
@decframe:
   dec frame
@noframe:
   jsr tvsetup
   ldy #$00
   sty temp2
   jsr waitvblank
   ldx frame
   cpx #$01
   bne @notdone
   
   tsx
   lda #$00
@clearloop:
   cpx #schedule
   beq @noclear
   sta $00,x
@noclear:
   dex
   cpx #$03
   bne @clearloop
   inc schedule
   bne @exit
@notdone:
   cpx #$10
   bcs @nomute
   sty AUDV0 ; Y should be $00 here
@nomute:
   cpx #$50
   bcs @disploop
   lda #$06 
   sta AUDC0
   txa
   ldx #$50
   sbc #$2f
   bpl @norev
   eor #$1f
@norev:
   sta AUDF0
   
@disploop:
   cpx #$90
   bcc @cleargfx
   lda $00,x
   .byte $24
@cleargfx:
   tya
   sta WSYNC
   inc temp2
   sta PF2
   txa
   asl
   tay
   and #$0f
   sta temp1
   lda (colortab),y
   and #$f0
   ora temp1
   ldy #$00
   sta COLUPF
   inx
   cpx #$ee ; last byte = $00 to clear PF2
   bcc @disploop

   ldx temp2
   dex
   dex
@waitloop:
   sta WSYNC
   inx
   cpx numlines
   bne @waitloop

@exit:
   jsr waitscreen
   jmp waitoverscan
