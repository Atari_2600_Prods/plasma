
.include "vcs.inc"
.include "globals.inc"

.segment "ZEROPAGE"
startx:
   .byte $00
starty:
   .byte $00
seedx:
   .byte $00
seedy:
   .byte $00
   
.segment "RODATA"

seeds:
   .word seed0,  seed1,  seed2,  seed3
   .word seed4,  seed5,  seed6,  seed7
   .word seed8,  seed9,  seed10, seed11
   .word seed12, seed13, seed14, seed15
   
.segment "CODE"
;.align 256

clearseed:
   lda #$00
   sta seedx
   sta seedy
   sta startx
   sta starty
   rts
   
selectseed:
   lda config
   and #$0f
   asl
   tax
   lda seeds,x
   sta temp1
   lda seeds+1,x
   sta temp2
   ldx seedx
   ldy seedy
   jmp (temp1)

seed0:
; x+=1, y+=1
   inc startx
   inc starty
   jmp selectplasma
   
seed1:
; x+=1, y-=1
   inc startx
   dec starty
   jmp selectplasma
   
seed2:
; x+=3, y+=2
   ldx #$03
   ldy #$02
   bne addup
   
seed3:
; x-=1, y+=2
   ldx #$ff
   ldy #$02
   bne addup
   
seed4:
; x+=2, y-=1
   ldx #$02
   ldy #$ff
   bne addup
   
seed5:
; x+=2, y+=3
   ldx #$02
   ldy #$03
addup:
   clc
   txa
   adc startx
   sta startx
addupy:
   clc
   tya
   adc starty
   sta starty
   jmp selectplasma
      
seed6:
; y+=3, x=(sin(++sx & 7f))
   inc seedx
   lda seedx
   and #$7f
   tax
   lda sinustab,x
   sta startx
   ldy #$03
   bne addupy

seed7:
; sx-=1, sy+=3, x=sin(sx), y=sin(sy)
   dec seedx
   inc seedy
   inc seedy
   inc seedy
   ldx seedx
   lda sinustab,x
   sta startx
   ldx seedy
   lda sinustab,x
   sta starty
   jmp selectplasma
   
seed8:
; sinus offset + 1/8
   ldy #$20
   .byte $2c
seed9:
; sinus offset + 1/4
   ldy #$40
   .byte $2c
seed10:
; sinus offset + 1/2
   ldy #$80
   inc seedx
   inc seedy
   ldx seedx
   lda sinustab,x
   sta startx
   tya
   clc
   adc seedy
   tax
   lda sinustab,x
   sta starty
   jmp selectplasma
   
seed11:
; sx-=1, sy+=3, x=sin(sx), y=sin(sy)
   dec seedx
   inc seedy
   inc seedy
   inc seedy
   ldx seedx
   ldy seedy
   lda sinustab,x
   sta startx
   lda sinustab,y
   sta starty
   jmp selectplasma 
   
seed12:
; unknown
   txa
   clc
   adc #$01
   sta seedx
   asl
   tax
   lda sinustab,x
   sta starty
   tax
   tya
   clc
   adc #$fe
   sta seedy
   tay
   lda sinustab,y
   lsr
   adc sinustab,x
   sta startx
   jmp selectplasma

seed13:
   inc seedx
   ldx seedx
   lda sinustab,x
   lsr
   lsr
   lsr
   lsr
   lsr
   lsr
   adc startx
   sta startx
   dec starty
   jmp seed0
   
seed14:
   inc seedx
   ldx seedx
   lda sinustab,x
   lsr
   lsr
   lsr
   lsr
   lsr
   lsr
   eor #$ff
   adc startx
   sta startx
   inc starty
   jmp seed0
   
seed15:
; unknown
   inc seedx
   inc seedy
   
   lda seedx
   bpl @nox
   eor #$ff
@nox:
   sta startx
   
   clc
   lda seedy
   adc #$40
   bmi @noy
   eor #$ff
@noy:
   sta starty
    
   jmp selectplasma
