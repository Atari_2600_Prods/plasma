
.include "vcs.inc"
.include "globals.inc"

; timer values for vblank and overscan are for TIM64TI, screen is for TIM1KTI
; PAL values, determined by trial an error
;.define TIMER_VBLANK   $2c
;.define TIMER_SCREEN   $11
;.define TIMER_OVERSCAN $2d

.segment "ZEROPAGE"

.segment "RODATA"

demoparts:
   .word splash-1, plasma-1

.segment "CODE"

reset:
.if 0
   ldx $D0                       ; determine if it's a 2600 or 7800
   cpx #$2C
   bne @not7800
   ldx $D1
   cpx #$A9
   bne @not7800
   cli   ; CLI = 7800
   .byte $24
@not7800:
   sei   ; SEI = 2600
.else
   sei
.endif
   cld
   lda #$00
   tax
   tay
@loop:
   pha
   txs
   inx
   bne @loop
   beq skipfirstoverscan
   
waitvblank:
   bit TIMINT
   bpl waitvblank
   sta WSYNC
   rts
   
waitscreen:
   lda timeroscan
   sta TIM64TI
   rts

waitoverscan:
   bit TIMINT
   bpl waitoverscan
skipfirstoverscan:   

   lda #%1110  ; each '1' bits generate a VSYNC ON line (bits 1..3)
@syncloop:
   sta WSYNC
   sta VSYNC   ; 1st '0' bit resets VSYNC, 2nd '0' bit exit loop
   lsr
   bne @syncloop   ; branch until VSYNC has been reset
   sta VBLANK
   sta COLUBK
   lda timerblank
   sta TIM64TI
   lda schedule
   asl
   tax
   lda demoparts+1,x
   pha
   lda demoparts,x
   pha
   rts
