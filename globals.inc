
; file that contains all symbols visible to other source files

; main.s
.globalzp schedule
.global   reset
.global   waitvblank
.global   waitscreen
.global   waitoverscan

; splash.s
.global   splash

; sinus.s
.global   sinustab

; colortable.s
.global   ntsctab
.global   paltab

; interface.s
.globalzp colortab
.globalzp pffb
.globalzp frame
.globalzp numlines
.globalzp temp1
.globalzp temp2
.globalzp config
.globalzp timerblank
.globalzp timeroscan
.global   plasma
.global   tvsetup
.global   displayconfig

; hexnumbers.s
.global   hexnumbers

; seeds.s
.globalzp seedx
.globalzp startx
.globalzp starty
.global   clearseed
.global   selectseed

; plasmas.s
.globalzp line
.global   selectplasma
